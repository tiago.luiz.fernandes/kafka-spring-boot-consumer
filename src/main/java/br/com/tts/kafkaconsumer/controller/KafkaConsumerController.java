package br.com.tts.kafkaconsumer.controller;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CountDownLatch;

@RestController
public class KafkaConsumerController {

    private static final Logger logger =
            LoggerFactory.getLogger(KafkaConsumerController.class);

    private final KafkaTemplate<String, Object> template;
    private final String topicName;
    private final int messagesPerRequest;
    private CountDownLatch latch;

    public KafkaConsumerController(
            final KafkaTemplate<String, Object> template,
            @Value("${tpd.topic-name}") final String topicName,
            @Value("${tpd.messages-per-request}") final int messagesPerRequest) {
        this.template = template;
        this.topicName = topicName;
        this.messagesPerRequest = messagesPerRequest;
    }

    /*
    @GetMapping("/hello")
    public String hello() throws Exception {
        latch = new CountDownLatch(messagesPerRequest);
        latch.await(2, TimeUnit.SECONDS);
        logger.info("All messages received");
        return "Hello Kafka!";
    }
    */

    /*
    @KafkaListener(topics = "TOPIC1", clientIdPrefix = "json",
            containerFactory = "kafkaListenerContainerFactory")
    public void listenAsObject(ConsumerRecord<String, PracticalAdvice> cr,
                               @Payload PracticalAdvice payload) {
        logger.info("Logger 1 [JSON] received key {}: Type [{}] | Payload: {} | Record: {}", cr.key(),
                typeIdHeader(cr.headers()), payload, cr.toString());
        latch.countDown();
    }*/

    /* Parametro @Payload String payload */
    /*
    ,
            containerFactory = "kafkaListenerStringContainerFactory"
     */
    @KafkaListener(topics = "${tpd.topic-name}", clientIdPrefix = "string")
    public void listenasString(ConsumerRecord<String, String> cr, Acknowledgment acknowledgment) {
        logger.info("Logger 2 [String] received key {}: | values: {} | offset: {}", cr.key(),cr.value(), cr.offset());

        acknowledgment.acknowledge();
        //latch.countDown();
    }

    /*
    @KafkaListener(topics = "TOPIC1", clientIdPrefix = "bytearray",
            containerFactory = "kafkaListenerByteArrayContainerFactory")
    public void listenAsByteArray(ConsumerRecord<String, byte[]> cr,
                                  @Payload byte[] payload) {
        logger.info("Logger 3 [ByteArray] received key {}: Type [{}] | Payload: {} | Record: {}", cr.key(),
                typeIdHeader(cr.headers()), payload, cr.toString());
        latch.countDown();
    }*/

    /*
    private static String typeIdHeader(Headers headers) {
        return StreamSupport.stream(headers.spliterator(), false)
                .filter(header -> header.key().equals("__TypeId__"))
                .findFirst().map(header -> new String(header.value())).orElse("N/A");
    }*/
}